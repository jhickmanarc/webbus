/*
 * Web Bus JavaScript component manager.
 * No external dependencies.
 * 
 * (c) 2014 - James Hickman
 * 
 * Liscenced under the Lesser GNU GPL (LGPL)
 * 
 * Component defined as a <DIV> identified by the class 'webbus_container'
 * The ID (must be unique) becomes the name of the object for the component.
 * Required custom property 'logic' that defines the function name of the constructor.
 * Optional property of 'params' contains a JS object litteral that is passed to the component constructor.
 * 
 * 
 * URL persistance is supported in the anchor-hash section of the URL string.
 * The data is concatinated as name value pairs delineated by a slash '/', the
 * first item in the slash deminimated string is the module name.
 * The data for each module is deliminated by the pipe charactor '|'.
 * Appropriate escaping is done for data.
 * 
 * With appropriate adapters routing in frameworks like EmberJS should integrate
 * and this shyould be more pleasing to the SEO URL obsessed croud.
 * 
 * 
 * Methods used by components:
 * 
 * webbus.get_params()
 * 		Deseralize the Query String and return  a JavScript object.
 * 
 * webbus.mixin(o_target, o_src)
 * 		Mix o_src into o_target. Nothing returned o_target modified.
 * 
 * webbus.fire_event(handler_name, data)
 * 		Call any handler of the specified name on any other Web Bus component
 * 		if the method exists.
 * 		Paramiters:
 * 			handler_name: String, name of the target method.
 * 			data: Object, any data to pass to the target,
 * 		Return:
 * 			Object with members named the matched component containing what was returned.
 *
 *
 ******************************************************************************************************************
 * Updates:
 *   - 2016-03-01 <James Hickman> Added support for path limited events.
 */
function webbus_init(pageConfig) {
	// TODO: Browser detect and die if obsolete browser being used. Sorry. Upgrade.
	
	/*
	 * Create the Web Bus component container and metadata.
	 */
	 if (window.hasOwnProperty('webbus') == false) {
		 window.webbus = {
			 'version': '0.1',
			 'name': 'Web Bus JavaScript component management and communication. (C)2014 - James Hickman',
			 'components': {},
			 'component_states': {},
			 'hash_flag': 0,
			 'page_config': pageConfig,
			 /*
			  * Framework Utility Methods
			  */
			 
			 /*
			  * Pack the variables set in the Query String 
			  * (URL part after the ?) as a JS object.
			  */
			 'get_params': function() {
				var params = {};
				if (location.search) {
					var parts = location.search.substring(1).split('&');

					for (var i = 0; i < parts.length; i++) {
						var nv = parts[i].split('=');
						if (!nv[0]) continue;
						params[nv[0]] = nv[1] || true;
					}
				}
				return params;
			 },
			 
			 /*
			  * Mix-In utility.
			  * Copies the elements from o_src into o_target
			  */
			'mixin': function(o_target, o_src) {
				for (prop in o_src) {
					o_target[prop] = o_src[prop];
				}
			 },
			 
			 /*
			  * Fire an event to components managed through WebBus,
			  * multicast method call.
			  * 
			  * Params:
			  * 		handler: String, Name of the method to call.
			  * 		data: Object, whatever we are passing to the handler.
			  * Returned:
			  * 		Object with members named the name of each called Module, member contains what was returned.
			  */
			 'fire_event': function(handler_path, data) {
			 	 var handler = '', path = '';
			 	 var split_at = handler_path.lastIndexOf('/');
			 	 if (split_at == -1) {
			 	 	handler = handler_path;
			 	 }
			 	 else {
			 	 	// Path specified
			 	 	path = handler_path.substring(0, split_at);
			 	 	handler = handler_path.substring(split_at + 1);
			 	 }
				 var results = {};
				 for (m in window.webbus.components) {
					if (m != null && typeof window.webbus.components[m] === 'object') {
						if (window.webbus.components[m][handler] != null && typeof window.webbus.components[m][handler] === 'function') {
							if (path == '' || window.webbus.components[m].dom_path.indexOf(path) != -1)
								results[m] = window.webbus.components[m][handler](data);
						}
					}
				 }
				 return results;
			 },
			 /*
			  * Save state dynamically in the hash string of the URL,
			  * then the specific state of the page can be bookmarked,
			  * linked, back/forward, etc.
			  */
			 'hash_decode': function() {
				 var urlp = window.location.href.split('#')[1];
				 this.component_states = {};
				 try {
					 if (urlp.length > 0) {
						 var cs = urlp.split('|');
						 for (n in cs) {
							 var values = cs[n].split('/');
							 var component_name = values[0];
							 this.component_states[component_name] = {};
							 for (var i = 1; i < values.length; i += 2) {
								 var k = values[i];
								 var v = values[i + 1];
								 // Unescape problem charactors.
								 v = v.replace(new RegExp(':AMPERSAND:', 'g'), '&');
								 v = v.replace(new RegExp(':HASH:', 'g'), '#');
								 v = v.replace(new RegExp(':PIPE:', 'g'), '|');
								 v = v.replace(new RegExp(':SPACE:', 'g'), ' ');
								 v = v.replace(new RegExp(':BACKSLASH:', 'g'), '\\');
								 v = v.replace(new RegExp(':SLASH:', 'g'), '/');
								 v = v.replace(new RegExp(':COLEN:', 'g'), ':');
								 this.component_states[component_name][k] = v;
							 }
						 }
					 }
				  }
				  catch(err) {
					  // Log a warning.
					  console.log("WARNING: Unable to parse URL string after the hash.");
				  }
			 },
			 'hash_encode': function(id, data) {
				 this.hash_flag = 1; // Flag to ignore change event when fired after the script changes the hash.
				 this.component_states[id] = data;
				 var cstates = [];
				 for (name in this.component_states) {
					 var parts = [];
					 parts.push(name);
					 for (k in this.component_states[name]) {
						 parts.push(k);
						 var v = this.component_states[name][k].toString();
						 // Escap charactors that cause problems with reparsing strings.
						 v = v.replace(new RegExp(':', 'g'), ':COLEN:');				//
						 v = v.replace(new RegExp('/', 'g'), ':SLASH:');				//
						 v = v.replace(new RegExp('\\\\', 'g'), ':BACKSLASH:');		//
						 v = v.replace(new RegExp(' ', 'g'), ':SPACE:');				//
						 v = v.replace(new RegExp('[|]', 'g'), ':PIPE:');				//
						 v = v.replace(new RegExp('#', 'g'), ':HASH:');				//
						 v = v.replace(new RegExp('&', 'g'), ':AMPERSAND:');			//
						 parts.push(v);
					 }
					 cstates.push(parts.join('/'));
				 }
				 var hashed = cstates.join('|');
				 var urlp = window.location.href.split('#');
				 window.location.href = urlp[0] + '#' + hashed;
			 },
			 'build_path': function(dom_element) {
			 	/*
			 	 * Construct path of parent containers with IDs.
			 	 */
			 	var path = [];
			 	var current = dom_element.parentElement;
			 	while (current.parentElement !== null) {
			 		if (current.id !== '') {
			 			path.unshift(current.id);
			 		}
			 		current = current.parentElement;
			 	}
			 	return path.join('/');
			 }
		 };
	 }
	 
	 /*
	  * Setup the URL hash On Change handler.
	  * Use flag to ignore the event when fired off after hash_encode() called.
	  */
	 window.onhashchange = function() {
		 if (webbus.hash_flag == 0) {
			 webbus.hash_decode();
			 for (component in webbus.components) {
				 webbus.components[component].component_state = webbus.component_states[component];
			 }
			 webbus.fire_event('_state_changed', {});
		 }
		 webbus.hash_flag = 0;
	 }
	 
	 /*
	  * Framework initilization operations.
	  * 
	  * Container definition in the HTML:
	  * 	<div class='webbus_container' logic='object_constructor_function_name' params="{'configuration':'JavaScripit object litteral'}"></div>
	  */
	  
	 /*
	  * Prototype object to be mixed into all Components
	  */
	 var base_component = {
		'container_dom': null,
		'component_name': '',
		'component_state': {},
		'dom_path': '',
		'set_state': function(value) {
			webbus.hash_encode(this.component_name, value);
		},
		'get_global_config': function(key) {
			return webbus.page_config[key];
		}
	 };
	 
	 // If there is any hash data to decode.
	 webbus.hash_decode();
	 
	 // Find all divs with the 'webbus_container' class:
	 //		get the 'logic' name
	 //		if a 'params' set desearalize the configuration object, else create an empty configuration object.
	 //		instanciate the component.
	 
	 var containers = document.getElementsByClassName('webbus_container');
	 for (var i = 0; i < containers.length; i++) {
		 var container = containers[i];
		 if (container.nodeName.toLowerCase() == 'div') {
			 var id = container.id;
			 var logic = container.getAttribute('LOGIC');
			 var params = container.getAttribute('PARAMS');
			 var param_o = {};
			 if (params != null && params.length > 0) {
				 try {
					param_o = eval('(' + params + ')');
				 }
				 catch(err) {
					 console.log("WebBus ERROR: Cannot parse params for '" + id + "'");
				 }
			 }
			 var component = new window[logic](param_o);
			 base_component.component_name = id;
			 base_component.container_dom = container;
			 base_component.dom_path = window.webbus.build_path(container);
			 if (window.webbus.component_states[id] != null) {
				 base_component.component_state = window.webbus.component_states[id];
			 }
			 else {
				 base_component.component_state = {};
			 }
			 window.webbus.mixin(component, base_component);
			 window.webbus.components[id] = component;
		 }
	 }
	 
	 // Brodcast a call to _setup() to make sure all components fully initilized.
	 // Broadcase a call to _start() for anything that has to be done once all modules have initilized.
	 window.webbus.fire_event('_setup', {});
	 window.webbus.fire_event('_start', {});
}
